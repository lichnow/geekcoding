<?php 
namespace Site\LessonBundle\Document;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="lesson_chapters",repositoryClass="Site\LessonBundle\Repository\ChapterRepository")
 * @JMS\ExclusionPolicy("all")
 */
class Chapter
{
    /**
     * @MongoDB\Id(strategy="INCREMENT")
     * @JMS\Groups({"getlesson"})
     * @JMS\Expose
     */
    protected $id;

    /** 
     * @MongoDB\ReferenceOne(targetDocument="Category",inversedBy="category")
     */
    private $category;

    /** @MongoDB\ReferenceOne(targetDocument="Video", mappedBy="chapter") */
    private $video;

    /**
     * @MongoDB\String @MongoDB\UniqueIndex
     * @JMS\Groups({"getlesson"})
     * @JMS\Expose
     */
    protected $name;

    /**
     * @MongoDB\String
     */
    protected $description = '';

    /**
     * @MongoDB\Int
     * @JMS\Groups({"getlesson"})
     * @JMS\Expose
     */
    protected $class;

    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param Site\LessonBundle\Document\Category $category
     * @return self
     */
    public function setCategory(\Site\LessonBundle\Document\Category $category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     *
     * @return Site\LessonBundle\Document\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set video
     *
     * @param Site\LessonBundle\Document\Video $video
     * @return self
     */
    public function setVideo(\Site\LessonBundle\Document\Video $video)
    {
        $this->video = $video;
        return $this;
    }

    /**
     * Get video
     *
     * @return Site\LessonBundle\Document\Video $video
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set class
     *
     * @param int $class
     * @return self
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * Get class
     *
     * @return int $class
     */
    public function getClass()
    {
        return $this->class;
    }
}
