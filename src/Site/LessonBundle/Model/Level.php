<?php
namespace Site\LessonBundle\Model;
use Site\CoreBundle\InterFacer\Model as BaseModel;
class Level extends BaseModel
{
	public function getAll()
    {
         $levels = $this->rp->findAll();
         return $levels;
    }
    public function getOneShow($id)
    {
        $level = $this->rp->findOneBy(array('id' => $id));
        return $level;
    }
}