<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Lesson
 *
 * @author lichnow
 */

namespace Site\LessonBundle\Model;
use Site\CoreBundle\InterFacer\Model as BaseModel;
class Lesson extends BaseModel
{
    public function getAll()
    {
         $lessons = $this->rp->findAll();
         return $lessons;
    }
    public function getAllByLearn()
    {
        $lessons = $this->rp->findAllByLearn();
        return $lessons;
    }
    public function getOneShow($index)
    {
        $lesson = $this->rp->findOneBy(array('index' => $index));
        return $lesson;
    }
    public function getCount()
    {
        return count($this->getAll()->toArray());
    }
    public function getList($skip = 0,$limit = 20){
        return $this->rp->findAllByOrder($skip,$limit);
    }
    public function getListByCondition($condition = array(),$skip = 0,$limit = 20){
        return $this->rp->findLimitByCondition($condition,$skip,$limit);
    }
    public function getCountByCondition($condition = array()){
        return $this->rp->getCountByCondition($condition);
    }
    public function getListByType($index,$skip = 0,$limit = 20)
    {
        if($index == null){
            return $this->getList($skip,$limit);
        }
        $type = $this->getModel()->get('SiteLessonBundle:Type')->getOneByIndex($index);
    	$lessons = $this->rp->findLimitByType($condition,$type,$skip,$limit);
    	return $lessons;
    }
    public function update($lesson){
        $nlesson = $this->rp->findOneByIndex($lesson->getIndex());
        if(!$nlesson)
            return false;
        $nlesson->setStatus($lesson->getStatus());
        $this->flush();
        return true;
    }

}

?>
