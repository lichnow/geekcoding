<?php

namespace Site\LessonBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Site\LessonBundle\Controller\BaseController as BaseController;

class PublicController extends BaseController
{
    /**
     * @Template("SiteLessonBundle:Public:subtypes.html.twig")
     */
    public function getSubTypesAction()
    {
        $data['subtypes'] = $this->LessonTypeModel->getGradeTypes(1);
        return $data;
    }
    public function getChindrenTypes($otypes,$ptype)
    {
    	$ctypes = array();
    	foreach ($otypes as $k => $v) {
        	$cpath = explode("-",$v->getPath());
        	if(count($cpath) == 2 && $cpath[1] == $ptype->getId()){
        		$ctypes[] = $v;
        	}
        }
        return $ctypes;
    }
}

