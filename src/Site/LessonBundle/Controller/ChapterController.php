<?php

namespace Site\LessonBundle\Controller;

use Site\LessonBundle\Controller\BaseController as BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
class ChapterController extends BaseController
{
	/**
	 * @Route("/chapter/get.html", name="site_chapter_get",options={"expose"=true})
     * @Template("SiteLessonBundle:Chapter:chapterget.html.twig",isStreamable=true)
     * @Method({"GET"})
     */
	public function getAction(){}
}