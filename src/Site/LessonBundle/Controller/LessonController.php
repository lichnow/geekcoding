<?php

namespace Site\LessonBundle\Controller;

use Site\LessonBundle\Controller\BaseController as BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\View;
class LessonController extends BaseController
{
	/**
	 * @Route("/show", name="site_lesson_show")
     * @Template("SiteLessonBundle:Public:layout.html.twig")
     */
	public function indexAction(){}

	/**
	 * @Route("/get.html", name="site_lesson_get",options={"expose"=true})
	 * @Template("SiteLessonBundle:Lesson:lessonget.html.twig",isStreamable=true)
     * @Method({"GET"})
     */
	public function getAction(){}

	/**
	 * @Route("/info.html", name="site_lesson_info",options={"expose"=true})
	 * @Template("SiteLessonBundle:Lesson:lessoninfo.html.twig",isStreamable=true)
     * @Method({"GET"})
     */
	public function infoAction(){}

	/**
	 * @Route("/chapterlist.html", name="site_lesson_chapterlist",options={"expose"=true})
     * @Template("SiteLessonBundle:Lesson:chapterlist.html.twig",isStreamable=true)
     * @Method({"GET"})
     */
	public function chapterListAction(){}
}