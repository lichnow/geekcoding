<?php

namespace Site\LessonBundle\Controller\Admin;
use Site\CoreBundle\InterFacer\AnnotationController;
class BaseController extends AnnotationController
{
	public function initialize(){
		$this->LessonModel = $this->getModel()->get('SiteLessonBundle:Lesson');
        $this->LevelModel = $this->getModel()->get('SiteLessonBundle:Level');
        $this->TypeModel = $this->getModel()->get('SiteLessonBundle:Type');
	}
}