<?php

namespace Site\LessonBundle\Controller\Admin;

use Site\LessonBundle\Controller\Admin\BaseController as BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
class LessonController extends BaseController
{
	/**
	 * @Route("/", name="lesson_admin_index")
	 * @Template("SiteLessonBundle:Admin:Lesson/index.html.twig")
     */
	public function indexAction(){
		$types = $this->TypeModel->getList();
		$ptypes = array();
		$ctypes = array();
		foreach ($types as $key => $value) {
			if(count(explode('-',$value->getPath())) == 2 )
			{
				array_push($ptypes,$value);
			}
			else if(count(explode('-',$value->getPath())) == 3)
			{
				$parentpath_arr = explode('-',$value->getPath());
				array_pop($parentpath_arr);
				$value->parentpath = implode("-", $parentpath_arr);
				array_push($ctypes,$value);
			}
		}
		$data = array(
			'levels' => $this->LevelModel->getAll(),
			'types' => array('ptypes' => $ptypes,'ctypes' => $ctypes)
		);
		return $data;
	}
}