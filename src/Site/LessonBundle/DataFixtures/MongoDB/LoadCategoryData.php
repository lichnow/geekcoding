<?php 
namespace Site\LessonBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Site\LessonBundle\Document\Category;

class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
    	$categories = array(
    		array('name' => '基础篇','order' => 1,'lesson' => 'symfony2','index' => 'basesymfony2'),
    		array('name' => '实战篇','order' => 2,'lesson' => 'symfony2' , 'index' => 'advancesymfony2'),
    		array('name' => '基础篇','order' => 1,'lesson' => 'laravel','index' => 'baselaravel4'),
    		array('name' => '实战篇','order' => 2,'lesson' => 'laravel' , 'index' => 'advancelaravel4')
    	);
    	$this->insertData($manager,$categories);
    }
    protected function insertData($manager,$categories)
    {
    	foreach ($categories as $key => $value) {
    		$category = new Category();
    		$category->setName($value['name']);
    		$category->setOrder($value['order']);
    		$category->setLesson($this->getReference($value['lesson'].'lesson'));
    		$manager->persist($category);
    		$manager->flush();
    		$this->addReference($value['index'].'category', $category);
    	}
    }
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 8;
    }
}