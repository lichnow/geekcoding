<?php

namespace Site\CoreBundle\Library\File\Local;

use Site\CoreBundle\InterFacer\Library;
class FileName extends Library
{
    public function getName($type = 'Date')
    {
    	$typename = 'generateBy'.$type;
    	return $this->$typename();
    }
    private function generateByDate()
    {
    	return date("Y/m-d");
    }
    private function generateByDateTime()
    {
    	return date("Y/m-d/H-i-s");
    }
    private function generateByRand()
    {
    	$randstr = '';
    	$str_length = 9;
        $arr = array_merge(range(0, 9), range('a', 'z'));
        $arr_len = count($arr);  
        for ($i = 0; $i < $str_length; $i++)  
        {  
            $rand = mt_rand(0, $arr_len-1);
            $randstr .= $arr[$rand]; 
        }  
        return $randstr;
    }
    private function generateByNumRand()
    {
    	return rand(0,1000);
    }	
}