<?php

namespace Site\CoreBundle\Library\File;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Site\CoreBundle\Library\File\Local\Snoopy;
use Site\CoreBundle\InterFacer\Library;
class Local extends Library
{
	public $root_path;
	public function __construct($container)
	{
		parent::__construct($container);
		$this->root_path = dirname($this->get('kernel')->getRootDir()).'/web/uploads';
	}
	public function createDir($dirname = null)
	{
		if($dirname == null)
			$dirname = $this->getFileClass('FileName')->getName();
		$dir = $this->root_path.'/'.$dirname;
		if(is_dir($dir))
			rmdir($dir);
		if(mkdir($dir,0777,true))
			return true;
		throw new AccessDeniedException('Can not create Directory '.$dir);
	}
	public function saveRemoteFile($remotefile,$suffix = null,$filename = null,$path = null)
	{
		if($path == null)
			$path = $this->getFileClass('FileName')->getName();
		if(!is_dir($this->root_path.'/'.$path))
			$this->createDir($path);
		if($suffix == null)
		{
            $ext_array = strrchr($remotefile,".");
            $suffix = $ext_array[1];
		}
        if($filename == null)
        {
        	do{
			    $filename = $this->getFileClass('FileName')->getName('Rand');
		    }while(file_exists($filename));
        }
		$newfile = $path.'/'.$filename.$suffix;
		$fp = fopen($this->root_path.'/'.$newfile,'wb');
        $ch = curl_init($remotefile);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
		return 'uploads/'.$newfile;
	}
	public function getFileClass($class)
	{
		$classname = 'SiteCoreBundle:File/Local/'.$class;
		return $this->getLibrary()->get($classname);
	}
}