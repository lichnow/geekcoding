<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author lichnow
 */
namespace Site\CoreBundle\InterFacer;
use Site\CoreBundle\InterFacer\Foundation;
abstract class Model extends Foundation
{
    public $dm;
    protected $rp = null;
    public function init($dm = null){
        $this->dm = $dm;
        $this->rp = $this->getCurrentRp();
        parent::init();
    }
    public function flush()
    {
        $this->dm->flush();
    }
    protected function getRp($rpname)
    {
        return $this->dm->getRepository($rpname);
    }
    private function getCurrentRp()
    {
        if($this->rp == null){
            $model_class = get_called_class();
            $model_arr = explode('\\',$model_class);
            return $this->getRp($model_arr[0].$model_arr[1].':'.$model_arr[count($model_arr)-1]);
        }else{
            return $this->getRp($this->rp);
        }
    }
    public function __call( $method , $args )  
    {  
        try{
            return call_user_func_array( array( $this->rp , $method ) , $args ); 
        }catch(\FatalErrorException $e){
            throw new $e;  
        }
    }
}

?>
