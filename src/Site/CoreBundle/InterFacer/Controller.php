<?php
 
namespace Site\CoreBundle\InterFacer;

use Site\CoreBundle\InterFacer\Foundation;
use Site\CoreBundle\InterFacer\InitControllerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

abstract class Controller extends Foundation implements InitControllerInterface
{
    public $init_param = array();
    public function __construct(){}
    public function init(){
        $baseinfo = $this->getModel()->get('SiteCoreBundle:System')->getShow();
        $this->setSession('title',$baseinfo->getTitle());
        $this->setSession('meta',$baseinfo->getMeta());
        $this->setSession('description',$baseinfo->getDescription());
        if(method_exists($this, 'initialize')){
            return call_user_func_array( array( &$this , 'initialize' ) , $this->init_param);
        }
    }
    public function createNamedForm($name, $type = 'form', $data = null, array $options = array())
    {
        return $this->container->get('form.factory')->createNamed($name,$type, $data, $options);
    }
    public function createNamedFormBuilder($name, $data = null, array $options = array())
    {
        return $this->container->get('form.factory')->createNamedBuilder($name,'form', $data, $options);
    }
    public function forward($controller, array $path = array(), array $query = array())
    {
        $path['_controller'] = $controller;
        $subRequest = $this->container->get('request')->duplicate($query, null, $path);

        return $this->container->get('http_kernel')->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
    }
    public function render($view, array $parameters = array(), Response $response = null)
    {
        return $this->container->get('templating')->renderResponse($view, $parameters, $response);
    }
    public function redirect($url, $status = 302)
    {
        return new RedirectResponse($url, $status);
    }
    public function renderView($view, array $parameters = array())
    {
        return $this->container->get('templating')->render($view, $parameters);
    }
    public function createNotFoundException($message = 'Not Found', \Exception $previous = null)
    {
        return new NotFoundHttpException($message, $previous);
    }
    public function stream($view, array $parameters = array(), StreamedResponse $response = null)
    {
        $templating = $this->container->get('templating');

        $callback = function () use ($templating, $view, $parameters) {
            $templating->stream($view, $parameters);
        };

        if (null === $response) {
            return new StreamedResponse($callback);
        }

        $response->setCallback($callback);

        return $response;
    }
    public function createForm($type, $data = null, array $options = array())
    {
        return $this->container->get('form.factory')->create($type, $data, $options);
    }
    public function createFormBuilder($data = null, array $options = array())
    {
        return $this->container->get('form.factory')->createBuilder('form', $data, $options);
    }
}