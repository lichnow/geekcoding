<?php

namespace Site\CoreBundle\Controller\Admin;

use Site\LessonBundle\Controller\BaseController as BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
class SystemController extends BaseController
{
	/**
	 * @Route("/system", name="core_admin_index")
	 * @Template("SiteCoreBundle:Admin:System/index.html.twig")
     */
	public function indexAction(){}
}