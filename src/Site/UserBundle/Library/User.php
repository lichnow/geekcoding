<?php
namespace Site\UserBundle\Library;
use Symfony\Component\Security\Core\SecurityContext;
use Site\UserBundle\Form\Type\OauthCreateFormType;
use Site\CoreBundle\InterFacer\Library;
class User extends Library{
	public function checkLogin()
	{
		$islogin = ($this->getUser() == null) ? false : true;
		return $islogin;
	}
    public function checkPassword($userprofile,$password,$isemail = true)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('username' => $userprofile));
        if(count($user) < 1)
        {
            if($isemail == true)
            {
                $user = $userManager->findUserBy(array('email' => $userprofile));
                if(count($user) < 1)
                    return false;
            }else{
                return false;
            }
        }
        $encoder_service = $this->get('security.encoder_factory');
        $encoder = $encoder_service->getEncoder($user);
        $encoded_pass = $encoder->encodePassword($password, $user->getSalt());
        if($user->getPassword() == $encoded_pass)
            return true;
        else
            return false;
    }
    public function getUserimg()
    {
        if($user = $this->getUser())
        {
            if($user->getUserimg() != null)
                return $user->getUserimg();
            else if($user->getUserimg() == null && $user->getEmail() != null){
                $userimg = $this->getGravatar($user->getEmail());
                if($userimg != false)
                    return $userimg;
            }
        }
        return '@SiteUserBundle/Resources/images/gravatar.jpeg';
    }
    public function createOauthimg($user,$oauthuser = null)
    {
        if($oauthuser != null && $oauthuser->getEmail() != null && $user->getUserimg() == null){
            $remoteimg_new = $this->getGravatar($user->getEmail());
            $remoteimg_old = $this->getGravatar($oauthuser->getEmail());
            if($remoteimg_new){
               $filename = $this->getLibrary()->get('SiteCoreBundle:File')->saveRemoteFile($remoteimg_new,'.jpg');
               return $filename; 
            }
            if($remoteimg_old){
               $filename = $this->getLibrary()->get('SiteCoreBundle:File')->saveRemoteFile($remoteimg_old,'.jpg');
               return $filename; 
            }
        }
        return false;
    }
    public function getGravatar($email, $default = null,$size = '80', $rating = 'g',$secure = false){
        if($this->get('gravatar.api')->exists($email))
            return $this->get('gravatar.api')->getUrl($email,$size,$rating,$default,$secure);
        return false;
    }
}