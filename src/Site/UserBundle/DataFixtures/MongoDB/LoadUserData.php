<?php 
namespace Site\HomeBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $users = array(
            array(
                'username' => "lichnow",
                'password' => '2793667r',
                'email' => 'lichnow@gmail.com',
                'roles' => array('ROLE_SUPER_ADMIN'),
                'realname' => '翁宇杰',
                'enabled' => true
            ),
            array(
                'username' => 'test',
                'password' => '1234567q',
                'email' => '4068793210@qq.com',
                'enabled' => true,
                'roles' => array('ROLE_USER')
            )
        );
        $this->insertData($manager,$users);
    }

    protected function insertData($manager,$users)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        foreach ($users as $key => $value) {
            $user = $userManager->createUser();
            $user->setUserName($value['username']);
            $user->setPlainPassword($value['password']);
            $user->setEmail($value['email']);
            $user->setRoles($value['roles']);
            $user->setEnabled($value['enabled']);
            if(isset($value['realname'])){
                $user->setRealname($value['realname']);
            }
            $userManager->updateUser($user);
            $manager->flush();
            $this->addReference($value['username'].'user', $user);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
}
 ?>