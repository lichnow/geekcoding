<?php
namespace Site\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class MenuBuilder
{
    private $factory;
    private $container;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct($container,FactoryInterface $factory)
    {
        $this->factory = $factory;
        $this->container = $container;
    }

    public function createSidebarMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('adminhome', array('route' => 'admin_homepage','label' => '首页'))
        ->setAttribute('icon' , 'eye-open');

        $menu->addChild('system', array('route' => 'core_admin_index','label' => '系统'))
        ->setAttribute('icon' , 'envelope-alt');

        /**
        *
        *用户管理菜单
        **/
        $menu->addChild('user', array('uri' => '#','label' => '用户'))
        ->setAttribute('icon' , 'beaker');
        $menu['user']->addChild('user-add', array('uri' => '#','label' => '添加用户' ));
        $menu['user']->addChild('user-group', array('uri' => '#','label' => '用户组管理' ));
        $menu['user']->addChild('user-user', array('uri' => '#','label' => '用户管理' ));
        $menu['user']->addChild('user-role', array('uri' => '#','label' => '权限设置' ));

        $menu->addChild('lesson', array('uri' => '#','label' => '课程'))
        ->setAttribute('icon' , 'pencil');
        $menu['lesson']->addChild('lesson-type', array('uri' => '#','label' => '分类管理' ));
        $menu['lesson']->addChild('lesson-level', array('uri' => '#','label' => '级别管理' ));
        $menu['lesson']->addChild('lesson-lesson', array('route' => 'lesson_admin_index','label' => '课程管理' ));
        $menu['lesson']->addChild('lesson-chapter', array('uri' => '#','label' => '课时管理' ));
        $menu['lesson']->addChild('lesson-tag', array('uri' => '#','label' => '标签管理' ));
        $menu['lesson']->addChild('lesson-stack', array('uri' => '#','label' => '问答管理' ))
            ->setAttribute('badge' , array('type' => null,'number' => 18));

        $menu->addChild('shop', array('uri' => '#','label' => '财务'))
        ->setAttribute('icon' , 'tasks');
        $menu['shop']->addChild('shop-set', array('uri' => '#','label' => '支付设置' ));
        $menu['shop']->addChild('shop-any', array('uri' => '#','label' => '数据分析' ));
        $menu['shop']->addChild('shop-order', array('uri' => '#','label' => '订单管理' ));

        $menu->addChild('data', array('uri' => '#','label' => '备份'))
        ->setAttribute('icon' , 'time');

        return $menu;
    }
    public function createUserMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');
        $menu->addChild('sitehome', array('uri' => '#','label' => '预览网站'));
        $menu->addChild('profile', array('uri' => '#','label' => '账户设置'));
        $menu->addChild('message', array('uri' => '#','label' => '私信夹'))
        ->setAttribute('badge' , array('type' => 'bg-danger','number' => 3));
        $menu->addChild('loginout', array('uri' => '#','label' => '注销' ))
        ->setAttribute('before' , "<li class='divider'></li>");

        return $menu;
    }
}