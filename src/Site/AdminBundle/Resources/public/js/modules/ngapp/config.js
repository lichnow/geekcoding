define(['angular','angularresource','angularui','./depend'], function (angular) {
    var config = angular.module('ngapp', ['ngResource','ui.router','lesson'])
        .config(['$interpolateProvider', '$locationProvider',function ($interpolateProvider, $locationProvider) {
            $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
            // $locationProvider.html5Mode(true);
        }]).run(["$rootScope", "$location","$injector","$state",function($rootScope, $location,$injector,$state){
        	loaderService = $injector.get("loaderService");
        }]);
        return config;
});
