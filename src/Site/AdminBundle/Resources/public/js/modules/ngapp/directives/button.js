define([
	'../config'
], function (config) {
    config.directive('btnRadio', function() {
	    return {
	    	restrict: "A",
	    	link: function(scope, element, attrs) {
		    	$(document).on('click',$(element),function(event){
		    		event.preventDefault();
		    		$(this).prop('checked', true);
		    		scope.$apply(function(){
		    			var value = $("input[name="+attrs.name+"]:checked").val();
		    			scope[attrs.btnRadio] = value;
			        });
		    	});
		    }
	    }
	});
});