define([
	'../config'
], function (config) {
    config.directive('noLink', function() {
	    return function(scope, element, attrs) {
	    	$(document).on('click',$(element),function(event){
	    		event.preventDefault();
	    	});
	    }
	});
});