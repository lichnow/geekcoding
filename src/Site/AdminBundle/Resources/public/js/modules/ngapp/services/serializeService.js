define(['../config'], function (config) {
	config.factory('serializeService',[function () {
		return {
			jsonToArray:function(json){
				var array = [];
				$.each(json,function(index,item){
					array.push(item);
				});
				return array;
			}
		}
	}]);
});