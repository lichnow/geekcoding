define([
	'./config',
	'./routes/routes',
	'./services/services',
	'./directives/directives',
	'./controllers/controllers'],function(config){
    return config;
});