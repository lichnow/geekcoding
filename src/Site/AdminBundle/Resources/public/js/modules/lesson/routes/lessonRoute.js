define(['../config'], function (config) {
    config.config(['$stateProvider','$urlRouterProvider',function ($stateProvider,$urlRouterProvider) {
        $urlRouterProvider.when('/lessons', ['$state',function ($state) {
            $state.transitionTo('lesson',{page:1,level:'all',type:'all',status:'all'},{location:false});
        }]);
        $stateProvider
        .state('lesson', {
            url: '/lesson/list?page&level&type&status',
            templateUrl: Routing.generate('site_lesson_admin_list'),
            controller: 'lessonCtrl',
            resolve: {
                levels: function(viewService,$stateParams) {
                    return viewService.query('levelService');
                },
                types: function(viewService,$stateParams) {
                    return viewService.query('typeService');
                },
                lessons: function (viewService,$stateParams) {
                    return viewService.query('lessonService',{
                        index:'all',
                        page:$stateParams.page,
                        level:$stateParams.level,
                        type:$stateParams.type,
                        status:$stateParams.status
                    });
                }
            }
        });
    }]);
});