define(['../config'], function (config) {
    config.factory('levelService', ['$resource', function ($resource) {
        return $resource(Routing.generate('api_lesson_level_get') + '/:id', {id: '@id'}, {
            query: {method: 'GET', params: {id:'all'}, isArray: true},
        });
    }]);
});