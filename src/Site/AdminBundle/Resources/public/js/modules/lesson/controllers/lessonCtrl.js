define(['../config'], function (config) {
    config.controller('lessonCtrl', ['$scope',"levels","types","lessons","$stateParams","viewService",'serializeService',
     function ($scope, levels ,types,lessons,$stateParams,viewService,serializeService) {
     	$scope.levels = serializeService.jsonToArray(levels);
        $scope.types = serializeService.jsonToArray(types);
        $scope.lessons = [];
        $scope.pageCount = lessons.pagecount;
        $scope.lessons = serializeService.jsonToArray(lessons.lessons);
        $scope.statusLesson = $scope.lessons[0];
        $scope.statusChange = function(lesson,status){
            $scope.statusLesson = lesson;
            $scope.statusLesson.status = status;
        }
        $scope.currentStatus = 'all';
        $scope.getPageCount = function(count){return new Array(count);};
        $scope.setSel = {
        	setLevel: function(level){
                $scope.selectors.currentLevel = (level == 'all') ? {id:'all'} : level;
        	},
            setType: function(type){
                $scope.selectors.currentType = (type == 'all') ? {index:'all'} : type;
            }
        }
        $scope.selectors = {
        	currentLevel: {id:$stateParams.level},
            currentType: {index:$stateParams.type}       
        }
        $scope.flush = function(){
            viewService.query('lessonService',{
                    index:'all',
                    page:1,
                    level:$scope.selectors.currentLevel.id,
                    type:$scope.selectors.currentType.index,
                    status:$scope.currentStatus
                }).then(function(lessons){
                    $scope.lessons = serializeService.jsonToArray(lessons.lessons);
                    $scope.pageCount = lessons.pagecount;
                }
            );
        }
        $scope.$watch('[selectors,currentStatus]',function(newValue,oldValue,scope){
        	if(newValue == oldValue){
        		return;
        	}
            $scope.flush();
        },true);
        $scope.$watch('statusLesson',function(n,o,scope){
            if(n == o){ return; }
            viewService.save('lessonService',$scope.statusLesson).then(function(){
                $scope.flush();
            });
        },true);
    }]);
});