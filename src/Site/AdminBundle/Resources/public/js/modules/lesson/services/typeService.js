define(['../config'], function (config) {
    config.factory('typeService', ['$resource', function ($resource) {
        return $resource(Routing.generate('api_lesson_type_get') + '/:index', {index: '@index'}, {
            query: {method: 'GET', params: {index:'all'}, isArray: true},
        });
    }]);
});