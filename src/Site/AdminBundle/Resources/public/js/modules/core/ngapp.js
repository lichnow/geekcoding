define(['angular','jquery', '../ngapp/init'], function (angular, $) {
    angular.element(document).ready(function() {
        angular.bootstrap($('body'), ['ngapp']);
    });
});
