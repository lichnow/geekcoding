<?php

namespace Site\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends BaseController
{

	/**
     * @Route("/",name="admin_homepage")
     * @Template
     */
    public function indexAction(){}
}
