<?php
namespace Site\MemberBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class PublicController extends BaseController
{
    public function menuAction()
    {
        return $this->render('SiteMemberBundle:Public:menu.html.twig');
    }
}