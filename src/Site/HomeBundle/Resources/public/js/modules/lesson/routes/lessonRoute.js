define([
    '../config',
    '../../core/loader',
    '../controllers/lessonCtrl'
], function (config,loader) {
    config.config(['$stateProvider',function ($stateProvider) {
        $stateProvider
        .state('lesson', {
            url: '/get/{index}',
            views: {
                "@": {
                    templateUrl: Routing.generate('site_lesson_get'),
                    controller: 'lessonCtrl',
                },
                "chapter-list@lesson": {
                    templateUrl: Routing.generate('site_lesson_chapterlist'),
                },
                "lesson-info@lesson": {
                    templateUrl: Routing.generate('site_lesson_info'),
                },
            },
            // resolve: {
            //     lesson: function (viewService,$stateParams) {
            //         return viewService.get('lessonService',{index: $stateParams.index + 'a'});
            //     }
            // }
        });
    }]);
});