define([
    '../config',
    './lessonRoute',
    '../controllers/chapterCtrl',
    '../services/chapterService'
], function (config) {
    config.config(['$stateProvider',function ($stateProvider) {
        $stateProvider
        .state('lesson.chapter', {
            url: '/chapter/{chapter}',
            views: {
                "@": {
                    templateUrl: Routing.generate('site_chapter_get'),
                    controller: 'chapterCtrl'
                }
            }
        });
    }]);
});