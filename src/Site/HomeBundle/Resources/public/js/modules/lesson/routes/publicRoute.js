define([
    '../config',
], function (config) {
    config.config(['$stateProvider',function ($stateProvider) {
        $stateProvider
        .state('public',{
            abstruct: true,
            views: {
                "@": {
                    templateUrl:function (){
                        return Routing.generate('site_lesson_get');
                    }
                }
            }
        });
    }]);
    return config;
});