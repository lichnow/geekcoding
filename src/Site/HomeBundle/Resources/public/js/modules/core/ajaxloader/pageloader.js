	define(function(require, exports, module) {
		var $ = require('jquery');
		return {
			show: function(selector,toptype,lefttype,imagetype){
				toptype = arguments[1] ? arguments[1] : 'window';
				lefttype = arguments[2] ? arguments[2] : 'parent';
				imagetype = arguments[3] ? arguments[3] : 'bar';
				loaderclass = "." + imagetype + '-loader';
				var pageloader = $(loaderclass).eq(0).clone(true);
				$(loaderclass).remove();
				$(selector).prepend(pageloader);
				for(var $i = 0;$i < $(loaderclass).length;$i++){
					pageloader = $(loaderclass).eq($i);
					csstop = ($(window).height() - pageloader.height())/2 - $('.navbar').height() - parseInt($('#main > .container').css('padding-top')) + 'px';
					marginTop = parseInt(pageloader.closest('.loader-container').height() - pageloader.height())/2 + 'px';
					cssleft = parseInt($(window).width() - pageloader.width())/2 + 'px';
					marginLeft = parseInt(pageloader.closest('.loader-container').width() - pageloader.width())/2 + 'px';
					if(toptype == 'window'){
						pageloader.css('top',csstop);
					}else if(toptype == 'parent'){
						pageloader.css('margin-top',marginTop);
					}
					if(lefttype == 'window'){
						pageloader.css('left',cssleft);
					}else if(lefttype == 'parent'){
						pageloader.css('margin-left',marginLeft);
					}
					if(toptype == 'parent' && lefttype == 'parent'){
						pageloader.css('position','relative');
					}else{
						pageloader.css({'position':'absolute','z-index':1000});
					}
				}
				$(loaderclass).show();
			},
			hide: function(imagetype){
				imagetype = arguments[0] ? arguments[0] : 'bar';
				loaderclass = "." + imagetype + '-loader';
				if($(loaderclass).length > 0){
					var pageloader = $(loaderclass).eq(0).clone(true);
					$(loaderclass).remove();
					pageloader.hide();
					$("#main").append(pageloader);
				}
			},
			isShow: function(imagetype){
				imagetype = arguments[0] ? arguments[0] : 'bar';
				loaderclass = "." + imagetype + '-loader';
				var isShow = false;
				for(var $i = 0;$i < $(loaderclass).length;$i ++){
					if($(loaderclass).eq($i).is(":visible")){
						isShow = true;
					}
				}
				return isShow;
			}
		}
	});