define(['../config'], function (config) {
    config.factory('planService', ['$resource' , function ($resource) {
        return $resource(Routing.generate('api_lesson_lesson_get', {index: 'sencha-touch'}), {}, {
            query: {method: 'GET', params: {}, isArray: false}
        });
    }]);
});