define([
    './globalRoute',
    '../controllers/lessonCtrl',
    '../services/lessonService'
], function (config) {
    config.config(function ($stateProvider) {
        $stateProvider
            .state('user_home.lesson', {
                url: "^"+Routing.generate('site_user_template_lesson'),
                views: {
                    "user-content": {
                        templateUrl: Routing.generate('site_user_template_lesson'),
                        controller: 'lessonCtrl',
                        resolve: {
                            lessoninfo: function (viewService) {
                                return viewService.getData('lessonService');
                            }
                        }
                    }
                }
            });
    });
});