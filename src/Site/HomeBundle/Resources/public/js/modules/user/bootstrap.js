define(['angular', './routes/routes', './config','jquery'], function (angular, routes, config,$) {
	angular.element(document).ready(function() {
        angular.bootstrap($('.user-container'), ['user']);
    });
});
