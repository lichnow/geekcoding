define([
    '../config',
], function (config) {
    var routing = {
        menu: Routing.generate('site_user_template_menu'),
        layoutcontent: Routing.generate('site_user_template_layoutcontent')
    };
    config.config(function ($stateProvider) {
        $stateProvider
            .state('user_home', {
                abstruct: true,
                views: {
                    "@": {
                        templateUrl: routing.layoutcontent,
                    },
                    "user-menu@user_home": {
                        templateUrl: routing.menu
                    }
                }
            });
    });
    return config;
});