<?php

namespace Site\HomeBundle\Controller;
use Site\CoreBundle\InterFacer\AnnotationController;
class BaseController extends AnnotationController{
	public $HomeModel;
	public $NavigateModel;
	public $LessonVideoModel;
	public $LessonTypeModel;
	public $LessonModel;
	public $UserLib;
	public function initialize()
	{
		$this->NavigateModel = $this->getModel()->get('SiteHomeBundle:Navigate');
		$this->LessonVideoModel = $this->getModel()->get('SiteLessonBundle:Video');
		$this->LessonTypeModel = $this->getModel()->get('SiteLessonBundle:Type');
		$this->LessonModel = $this->getModel()->get('SiteLessonBundle:Lesson');
	}
}