<?php

namespace Api\LessonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations\View;
use Site\CoreBundle\InterFacer\AnnotationController as BaseController;

/** 
* @Route("/level")
*/
class LevelController extends BaseController
{
	protected $LevelModel;
	public function initialize()
	{
		$this->LevelModel = $this->getModel()->get('SiteLessonBundle:Level');
	}

	/**
     * @View(serializerGroups={"levellist"})
     * @Route("/all",options={"expose"=true})
     * @Method({"GET"})
     */
    public function listAction()
    {
    	$levels = $this->LevelModel->getAll()->toArray();
    	return $levels;
    }

    /**
     * @View(serializerGroups={"levellist"})
     * @Route("/{id}",options={"expose"=true})
     * @Method({"GET"})
     */
    public function getAction($id)
    {
        $level = $this->LevelModel->getOneShow($id);
        return $level;
    }
}