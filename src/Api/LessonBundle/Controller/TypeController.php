<?php

namespace Api\LessonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations\View;
use Site\CoreBundle\InterFacer\AnnotationController as BaseController;

/** 
* @Route("/type")
*/
class TypeController extends BaseController
{
	protected $TypeModel;
	public function initialize()
	{
		$this->TypeModel = $this->getModel()->get('SiteLessonBundle:Type');
	}

	/**
     * @View(serializerGroups={"typelist"})
     * @Route("/all",options={"expose"=true})
     * @Method({"GET"})
     */
    public function listAction()
    {
    	$types = $this->TypeModel->getList()->toArray();
    	return $types;
    }

    /**
     * @View(serializerGroups={"typelist"})
     * @Route("/{id}",options={"expose"=true})
     * @Method({"GET"})
     */
    public function getAction($index)
    {
        $type = $this->TypeModel->getOneByIndex($index);
        return $type;
    }
}