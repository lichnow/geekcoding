<?php

namespace Api\LessonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Site\LessonBundle\Document\Lesson;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\View;
use Site\CoreBundle\InterFacer\AnnotationController as BaseController;

/** 
* @Route("/lesson",service="api_lesson.lessons")
*/
class LessonController extends BaseController
{
	protected $LessonModel;
    protected $levelModel;
    protected $typeModel;
	public function initialize()
	{
		$this->LessonModel = $this->getModel()->get('SiteLessonBundle:Lesson');
        $this->LevelModel = $this->getModel()->get('SiteLessonBundle:Level');
        $this->TypeModel = $this->getModel()->get('SiteLessonBundle:Type');
	}
	/**
     * @View(serializerGroups={"getlesson"})
     * @Route("/all",options={"expose"=true})
     * @Method({"GET"})
     */
    public function listAction()
    {
        $condition = array();
        $level_id = $this->getRequest()->get('level');
        $type_index = $this->getRequest()->get('type');
        $condition['status'] = ($this->getRequest()->get('status') == 'all') ? null : $this->getRequest()->get('status');
        $condition['level'] = ($level_id == 'all') ? null : $this->LevelModel->getOneShow($level_id);
        $condition['type'] = ($type_index == 'all') ? null : $this->TypeModel->getOneByIndex($type_index);
        $limit = ($this->getRequest()->get('limit') != null) ? $this->getRequest()->get('limit'):10;
        $current_page = ($this->getRequest()->get('page') != null) ? $this->getRequest()->get('page'):1;
        $page_start = ($current_page-1)*$limit;
        $pagecount = ceil($this->LessonModel->getCountByCondition($condition)/$limit);
    	$lessons = $this->LessonModel->getListByCondition($condition,$page_start,$limit)->toArray();
    	return array("pagecount" => $pagecount,"lessons" => $lessons);
    }
    /**
     * @View(serializerGroups={"getlesson"})
     * @Route("/{index}",options={"expose"=true})
     * @Method({"GET"})
     */
    public function getAction($index)
    {
    	$lesson = $this->LessonModel->getOneShow($index);
    	return $lesson;
    }

    /**
     * @Route("/{index}",options={"expose"=true})
     * @ParamConverter("lesson", converter="fos_rest.request_body", options={"deserializationContext"={"groups"={"editlesson"}}})
     * @Method({"PUT"})
     */
    public function putAction(Lesson $lesson)
    {
        $result = $this->LessonModel->update($lesson);
        return $data = ($result == true) ? array('result' => true) : array('result' => false);
    }
}
